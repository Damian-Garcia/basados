/**
 * @file EstablecerFecha.h
 * @brief This header defines the functions that update the date in the RTC
 */
#ifndef SOURCE_ESTABLECERFECHA_H_
#define SOURCE_ESTABLECERFECHA_H_

#include "stdbool.h"
#include "fsl_uart.h"
#include "FreeRTOS.h"

/**
 * @brief This function tries to update the date in the RTC
 * @param fecha ASCII string with the date in format dd/mm/aaaa
 * @param uart UART to print the results
 * @return true if it was possible to write, false otherwise
 */
bool EstablecerFecha_tryToUpdate(uint8_t * fecha, UART_Type* uart);

#endif /* SOURCE_ESTABLECERFECHA_H_ */
