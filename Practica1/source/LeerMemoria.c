/**
 * @file LeerMemoria.c
 * @brief This is the source file for the LeerMemoria header file
 */
#include "LeerMemoria.h"
#include "StringUtils.h"
#include "Formatter.h"
#include "Memoria.h"
#include "Mensajes.h"

bool leerMemoria(uint8_t *dir, uint8_t *cont, uint8_t ** buffer)
{

	bool res;

	uint32_t direccion, contenido;
	direccion = parseASCIIAsInt32(dir);
	contenido = parseASCIIAsInt32(cont);

	*buffer = pvPortMalloc(contenido);

	res = Memoria_leer(direccion, contenido, *buffer);

	return res;
}

bool LeerMemoria_tryToRead(uint8_t *dir, uint8_t *cont, UART_Type * currentUart)
{

	uint8_t * buffer;
	bool res = leerMemoria(dir, cont, &buffer);
	if (res)
	{
		imprimirMensaje(currentUart, buffer);
		imprimirMensajeExito(currentUart);
	}
	else
	{
		imprimirMensajeError(currentUart);
	}

	vPortFree(buffer);
	return res;
}
