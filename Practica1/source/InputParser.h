/**
 * @file InputParser.h
 * @brief This header defines the task that parse the input received in the uarts
 */
#ifndef SOURCE_INPUTPARSER_H_
#define SOURCE_INPUTPARSER_H_

#include "FreeRTOS.h"

/**
 * @brief this task accumulates and parses the input for UART0
 * The funtion stores to a buffer each character until an special
 * character arrives, like a cmd or esc. If the character was a
 * command, it sends the stored buffer to the menu. If the character
 * was esc, it sends the break message to the menu.
 */
void InputParser_parser1task(void * p);
/**
 * @brief this task accumulates and parses the input for UART4
 * The funtion stores to a buffer each character until an special
 * character arrives, like a cmd or esc. If the character was a
 * command, it sends the stored buffer to the menu. If the character
 * was esc, it sends the break message to the menu.
 */
void InputParser_parser2task(void * p);

/**
 * @brief enum for the different types of chars that can be received
 */
typedef enum
{
	INPUT_CHAR, INPUT_ENTER, INPUT_ESC
} INPUT_TYPE;


/**
 * @brief Function to get the type of a character.
 * @param entrada as the character to get its type
 * @return INPUT_TYPE of the character
 */
INPUT_TYPE InputParser_getTipo(uint8_t entrada);

#endif /* SOURCE_INPUTPARSER_H_ */
