/**
 * @file LeerHora.c
 * @brief This is the source file for the LeerHora header file
 */
#include "LeerHora.h"
#include "Mensajes.h"
#include "RTC.h"
#include "StringUtils.h"
#include "Formatter.h"

bool leerHoraRTC(UART_Type *currentUart)
{
	uint8_t s = 0, m = 0, h = 0, f = 0;
	uint8_t bs[5] =
	{ };
	uint8_t bm[5] =
	{ };
	uint8_t bh[5] =
	{ };

	Time t;

	bool res = RTC7940_readHora(&t);

	if (res)
	{
		imprimirTiempo(currentUart, &t);

		imprimirMensajeExito(currentUart);
	}
	return res;
}

bool LeerHora_tryToRead(UART_Type *currentUart)
{
	bool res = leerHoraRTC(currentUart);

	if (!res)
	{
		imprimirMensajeError(currentUart);
	}
}
