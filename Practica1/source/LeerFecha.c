/**
 * @file LeerFecha.c
 * @brief This is the source file for the LeerFecha header file
 */
#include "LeerFecha.h"
#include "RTC.h"
#include "Mensajes.h"
bool leerFechaRTC(UART_Type *currentUart)
{
	Date t;

	bool res = RTC7940_readFecha(&t);

	if (res)
	{
		imprimirFecha(currentUart, &t);

		imprimirMensajeExito(currentUart);
	}
	return res;
}

bool LeerFecha_tryToRead(UART_Type *currentUart)
{
	bool res = leerFechaRTC(currentUart);

	if (!res)
	{
		imprimirMensajeError(currentUart);
	}
}
