/**
 * @file LeerMemoria.h
 * @brief This header defines the functions for reading bytes from the external memory
 */
#ifndef SOURCE_LEERMEMORIA_H_
#define SOURCE_LEERMEMORIA_H_

#include "stdbool.h"
#include "fsl_uart.h"
#include "FreeRTOS.h"

/**
 * @brief This function tries to read the contents of the external memory
 * The function will try to communicate with the Memory via i2c
 * If the communication fails, the function will return false.
 * If communication was successful, it will print a success
 * message in the selected UART.
 * @param dir address to read on the memory, as an ASCII string
 * @param cont number of bytes to read, as an ASCII string
 * @param currentUart Uart to wire the message
 * @return true if read operation was successful, false otherwise
 */
bool LeerMemoria_tryToRead(uint8_t *dir, uint8_t *cont,
		UART_Type * currentUart);

#endif /* SOURCE_LEERMEMORIA_H_ */
