/**
 * @file UARTConfig.h
 * @brief This header defines an API to configure and use the UART communication module
 */
#ifndef SOURCE_UARTCONFIG_H_
#define SOURCE_UARTCONFIG_H_

#include "FreeRTOS.h"

#include "fsl_uart.h"

/**
 * @brief This function configures the UART communication module
 */
void UARTConfig_initUARTs();
/**
 * @brief Get the base UART of the first client
 * @return UART_Type
 */
UART_Type * UARTConfig_getUARTCliente1();
/**
 * @brief Get the base UART of the second client.
 * @return UART_Type
 */
UART_Type * UARTConfig_getUARTCliente2();

#endif /* SOURCE_UARTCONFIG_H_ */
