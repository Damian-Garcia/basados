/**
 * @file Pantalla.h
 * @brief This header defines an API to interface the LCD screen
 */

#ifndef SOURCE_PANTALLA_H_
#define SOURCE_PANTALLA_H_

#include "FreeRTOS.h"
#include "LCDNokia5110.h"
#include "IPC.h"

#include "semphr.h"

/**
 * @brief updates the contents of the LCD screen
 */
void printHoraPantalla();
/**
 * @brief This is the LCD main task.
 * If the screen is echoing it prints characters (updates the screen)
 * as son as they get inserted into the input queue.
 * If the screen is not echoing, the screen updates every 500 ms
 */
void PantallaUpdate_taks(void *p);
/**
 * @brief Stop the screen to update the time
 * Function is to indicate the screen to stop communicating to the
 * RTC, since the user is going to change the current time.
 */
void Pantalla_changeTime();
/**
 * @brief Stop the screen to update the date
 * Function is to indicate the screen to stop communicating to the
 * RTC, since the user is going to change the current date.
 */
void Pantalla_changeDate();
/**
 * @brief The user has input a new configuration. Update to the RTC.
 * This function should be called after the user finishes to change the
 * date to its desired value. It will apply the changes to the time and
 * date to the RTC.
 */
void Pantalla_resume();
/**
 * @brief Increment the value of the seconds or days depending.
 * If the user is configuring the time, calling this function will
 * increment the second count in the LCD local time. If the user
 * is configuring date, it will increment the day count.
 */
void Pantalla_incrementA();
/**
 * @brief Increment the value of the minutes or months depending.
 * If the user is configuring the time, calling this function will
 * increment the minutes count in the LCD local time. If the user
 * is configuring date, it will increment the months count.
 */
void Pantalla_incrementB();
/**
 * @brief Increment the value of the hour or year depending.
 * If the user is configuring the time, calling this function will
 * increment the hour count in the LCD local time. If the user
 * is configuring date, it will increment the year count.
 */
void Pantalla_incrementC();

/**
 *
 */
bool Pantalla_isListening();
void Pantalla_listen(bool l);
void Pantalla_stopListening();

#endif /* SOURCE_PANTALLA_H_ */
