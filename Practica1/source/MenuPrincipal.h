/**
 * @file MenuPrincipal.h
 * @brief This header defines the Menu task for interacting with the user
 */
#ifndef SOURCE_MENUPRINCIPAL_H_
#define SOURCE_MENUPRINCIPAL_H_

#include "stdbool.h"

#include "FreeRTOS.h"
#include "queue.h"

/**
 * @brief Main menu task
 * This task displays the menu to an uart and controls the operations
 * that the user can do.
 */
void Menu_taskMenu(void *p);

#endif /* SOURCE_MENUPRINCIPAL_H_ */
