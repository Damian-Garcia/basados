/**
 * @file InputParser.c
 * @brief This is the source file for the InputParser header file
 */
#include "InputParser.h"

#include "fsl_uart.h"

#include "Pantalla.h"
#include "queue.h"

#include "IPC.h"
#include "StringUtils.h"
#include "MenuPrincipal.h"
#define MAX_BUFFER_SIZE 1024

INPUT_TYPE InputParser_getTipo(uint8_t entrada)
{
	switch (entrada)
	{
	case '\e':
		return INPUTCHAR_ESC;
		break;
	case '\n':
		return INPUTCHAR_ENTER;
		break;
	default:
		return INPUTCHAR_CHAR;
		break;
	}
}

void appendToBuffer(uint8_t entrada, uint32_t * bufferIndex,
		uint8_t * inputBuffer)
{
	if (*bufferIndex < MAX_BUFFER_SIZE)
	{
		inputBuffer[*bufferIndex] = entrada;
		(*bufferIndex)++;
		inputBuffer[*bufferIndex] = '\0';
	}
}

void clearBuffer(uint32_t * bufferIndex, uint8_t * inputBuffer)
{
	inputBuffer[0] = '\0';
	*bufferIndex = 0;
}

void setOuputBuffer(uint32_t *bufferIndex, uint8_t * inputBuffer,
		uint8_t * outputBuffer)
{
	stringCopy(outputBuffer, inputBuffer);
	clearBuffer(bufferIndex, inputBuffer);
}

void sendBuffer(QueueHandle_t currentMenuQueue, uint32_t *bufferIndex,
		uint8_t *inputBuffer, InputMessage * salida)
{
	setOuputBuffer(bufferIndex, inputBuffer, salida->mensaje);
	QueueHandle_t colaSalida = currentMenuQueue;

	salida->codigo = INPUTMESSAGE_COMMAND;
	xQueueSend(colaSalida, salida, portMAX_DELAY);
}

void sendBreak(QueueHandle_t currentMenuQueue, uint32_t * bufferIndex,
		uint8_t * inputBuffer, InputMessage * salida)
{
	QueueHandle_t colaSalida = currentMenuQueue;
	clearBuffer(bufferIndex, inputBuffer);
	salida->codigo = INPUTMESSAGE_ESCAPE;
	xQueueSend(colaSalida, &salida, portMAX_DELAY);
}

void processNewChar(uint8_t entrada, QueueHandle_t currentMenuQueue,
		uint32_t * bufferIndex, uint8_t *inputBuffer, InputMessage *salida)
{
	INPUT_TYPE tipo;
	tipo = InputParser_getTipo(entrada);
	switch (tipo)
	{
	case INPUTCHAR_CHAR:
		appendToBuffer(entrada, bufferIndex, inputBuffer);
		break;
	case INPUTCHAR_ENTER:
		sendBuffer(currentMenuQueue, bufferIndex, inputBuffer, salida);
		break;
	case INPUTCHAR_ESC:
		sendBreak(currentMenuQueue, bufferIndex, inputBuffer, salida);
		break;
	}
}

extern Cliente *c1, *c2;

void InputParser_parser1task(void * p)
{
	uint8_t * entrada = pvPortMalloc(sizeof(uint8_t));

	uint32_t * bufferIndex;
	bufferIndex = pvPortMalloc(sizeof(uint32_t));
	*bufferIndex = 0;
	uint8_t * inputBuffer = pvPortMalloc(1024);
	uint8_t * outputBuffer = pvPortMalloc(1024);

	volatile Cliente *c = c1;

	QueueHandle_t parserQueue = c->inputQueue;
	QueueHandle_t menuQueue = c->menuQueue;

	InputMessage *salida = pvPortMalloc(sizeof(InputMessage));
	salida->mensaje = outputBuffer;

	for (;;)
	{
		xQueueReceive(c->inputQueue, entrada, portMAX_DELAY);

		if (*entrada == '\r')
		{
			continue;
		}

		processNewChar(*entrada, c->menuQueue, bufferIndex, inputBuffer,
				salida);
	}
}

void InputParser_parser2task(void * p)
{
	uint8_t * entrada = pvPortMalloc(sizeof(uint8_t));

	uint32_t * bufferIndex;
	bufferIndex = pvPortMalloc(sizeof(uint32_t));
	*bufferIndex = 0;
	uint8_t * inputBuffer = pvPortMalloc(1024);
	uint8_t * outputBuffer = pvPortMalloc(1024);

	volatile Cliente *c = (Cliente *) c2;

	QueueHandle_t parserQueue = c->inputQueue;
	QueueHandle_t menuQueue = c->menuQueue;

	InputMessage *salida = pvPortMalloc(sizeof(InputMessage));
	salida->mensaje = outputBuffer;

	for (;;)
	{
		xQueueReceive((*c).inputQueue, entrada, portMAX_DELAY);

		if (*entrada == '\r')
		{
			continue;
		}
		processNewChar(*entrada, c->menuQueue, bufferIndex, inputBuffer,
				salida);

	}
}

