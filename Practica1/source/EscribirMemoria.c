/**
 * @file EscribirMemoria.c
 * @brief This is the source file for the EscribirMemoria header file
 */
#include "EscribirMemoria.h"
#include "Mensajes.h"
#include "Memoria.h"
#include "StringUtils.h"
#include "Formatter.h"

bool escribirMemoria(uint8_t *dir, uint8_t *cont, uint8_t ** buffer)
{

	bool res;

	uint32_t direccion = parseASCIIAsInt32(dir);
	uint32_t cantidadDeBytes = stringLength(cont);

	*buffer = pvPortMalloc(cantidadDeBytes);

	res = Memoria_escribir(direccion, cantidadDeBytes, cont);

	return res;
}


bool EscribirMemoria_tryToWrite(uint8_t *dir, uint8_t *cont,
		UART_Type * currentUart)
{
	uint8_t *buffer;

	bool res = escribirMemoria(dir, cont, &buffer);
	if (res)
	{
		imprimirMensajeExito(currentUart);
	}
	else
	{
		imprimirMensajeError(currentUart);
	}
	vPortFree(buffer);

	return res;
}
