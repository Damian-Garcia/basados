/**
 * @file UARTConfig.c
 * @brief This is the source file for the UARTConfig header file
 */
#include "UARTConfig.h"

#include "fsl_uart.h"

#define UART_CLIENTE1 UART0
#define UART_CLIENTE2 UART4

void UARTConfig_initUARTs()
{
	uart_config_t config1, config2;
	UART_GetDefaultConfig(&config1);
	UART_GetDefaultConfig(&config2);

	config1.enableRx = true;
	config1.enableTx = true;
	config1.baudRate_Bps = 9600;

	config2.enableRx = true;
	config2.enableTx = true;
	config2.baudRate_Bps = 9600;

	uint32_t a = CLOCK_GetCoreSysClkFreq();
	uint32_t b = CLOCK_GetFreq(UART4_CLK_SRC);

	uint32_t c = a - b;

	UART_Init(UART_CLIENTE1, &config1, CLOCK_GetCoreSysClkFreq());
	UART_Init(UART_CLIENTE2, &config2, CLOCK_GetFreq(UART4_CLK_SRC));

	UART_EnableRx(UART_CLIENTE1, true);
	UART_EnableRx(UART_CLIENTE2, true);

	UART_EnableInterrupts(UART_CLIENTE1,
			kUART_RxDataRegFullInterruptEnable
					| kUART_RxOverrunInterruptEnable);
	UART_EnableInterrupts(UART_CLIENTE2,
			kUART_RxDataRegFullInterruptEnable
					| kUART_RxOverrunInterruptEnable);

}

UART_Type * UARTConfig_getUARTCliente1()
{
	return UART_CLIENTE1;
}

UART_Type * UARTConfig_getUARTCliente2()
{
	return UART_CLIENTE2;
}
