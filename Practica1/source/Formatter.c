/**	 \file Formatter.c
 \brief
 Este es el archivo fuente del formatter que contiene las distintas funciones
 que se utilizan para darle formato a las variables que son mostradas en pantalla,
 recibidas de la UART, enviadas a la UART, etc...
 \author Damian Garcia Serrano and Erick Ortega Prudencio
 \date	11/2/2016
 */
#include "Formatter.h"

#include "stdbool.h"

uint32_t tenPower(uint8_t n)
{
	int r = 1;
	for (; n; n--)
	{
		r *= 10;
	}
	return r;
}

uint8_t integerlength(uint32_t number)
{
	uint8_t digits;
	for (digits = 0; number; number /= 10, digits++)
		;
	return (digits > 0) ? digits : 1;
}

void formatUnsignedInteger32(uint32_t number, uint8_t *buffer, int8_t digits)
{
	uint8_t i = 0;
	if (digits < 0)
	{
		digits = integerlength(number);
	}

	for (i = 0; i < digits; i++)
	{
		buffer[digits - i - 1] = (number % 10) + '0';
		number /= 10;
	}
	buffer[digits] = '\0';
}

void formatfloat(float number, uint8_t *buffer, uint8_t decimalDigits)
{
	uint32_t integerPart;
	integerPart = number;
	uint8_t digits = integerlength(integerPart);
	formatUnsignedInteger32(number, buffer, digits);
	buffer[digits] = '.';
	number -= integerPart;
	number *= tenPower(decimalDigits);
	uint32_t decimalPart = number;
	formatUnsignedInteger32(number, buffer + digits + 1, decimalDigits);
}
void formatBCDint(uint32_t number, uint8_t * buffer, int8_t digits)
{
	uint8_t i = 0;
	if (digits < 0)
	{
		digits = integerlength(number);
	}
	uint32_t mask = 0xF;
	for (i = 0; i < digits; i++)
	{
		buffer[digits - i - 1] = (number & mask) + '0';
		number = number >> 4;
	}
	buffer[digits] = '\0';
}
uint16_t convertAsciiAddressToHexadecimal(uint8_t* buffer) //La posicion mas baja del buffer contiene el dato mas significativo
{
	uint16_t resultado = 0;
	uint8_t temp = 0;
	for (int i = 0; i < 4; i++)
	{
		temp = convertAsciiDigitToBinary(buffer[i]);
		resultado = resultado | temp << (12 - i * 4);
	}
	return resultado;
	//return ((convertAsciiToBinary(buffer[0])<<12) | (convertAsciiToBinary(buffer[1])<<8) | (convertAsciiToBinary(buffer[2])<<4) | (convertAsciiToBinary(buffer[3])<<0));
}
uint16_t convertAsciiAddressToHexadecimalBCDWithNumberOfDigits(uint8_t* buffer,
		uint8_t digits) //La posicion mas baja del buffer contiene el dato mas significativo
{
	uint16_t resultado = 0;
	uint8_t temp = 0;
	for (int i = 0; i < digits; i++)
	{
		temp = convertAsciiDigitToBinary(buffer[i]);
		resultado = resultado | temp << (((12 - 4 * (4 - digits)) - i * 4));
	}
	return resultado;
	//return ((convertAsciiToBinary(buffer[0])<<12) | (convertAsciiToBinary(buffer[1])<<8) | (convertAsciiToBinary(buffer[2])<<4) | (convertAsciiToBinary(buffer[3])<<0));
}
uint16_t convertAsciiBufferToOneUnsignedInt(uint8_t* buffer, uint8_t digits) //La posicion mas baja del buffer contiene el dato mas significativo
{
	uint16_t resultado = 0;
	uint8_t temp = 0;
	for (int i = 0; i < digits; i++)
	{
		temp = convertAsciiDigitToBinary(buffer[i]);
		resultado = resultado | temp << (((12 - 4 * (4 - digits)) - i * 4));
	}
	return convertBCDtoBinary(resultado);
	//return ((convertAsciiToBinary(buffer[0])<<12) | (convertAsciiToBinary(buffer[1])<<8) | (convertAsciiToBinary(buffer[2])<<4) | (convertAsciiToBinary(buffer[3])<<0));
}
uint8_t convertAsciiDigitToBinary(uint8_t value)
{
	if (value > 0x39)
	{
		return value - 55;
	}
	else
	{
		return value - 0x30;
	}
}

bool isDigit(uint8_t c)
{
	if ('0' > c || '9' < c)
	{
		return false;
	}
	else
	{
		return true;
	}
}

int32_t parseAsciiToUnsignedInt(uint8_t *buffer, uint8_t digits)
{
	uint32_t i = 0;
	uint32_t acum = 0;
	for (i = 0; i < digits; i++)
	{
		if (!isDigit(buffer[i]))
		{
			return -1;
		}
		acum *= 10;
		acum += buffer[i] - '0';
	}
	return acum;
}

int32_t parseASCIIAsInt32(uint8_t * s)
{
	int32_t res = 0;
	uint32_t mul = 1;

	uint32_t contador = 0;

	if (s == NULL)
	{
		return 1 << 31;
	}

	uint8_t inicial = s[0];

	contador += ('-' == inicial) ? 1 : 0;

	while (s[contador])
	{
		if (!isDigit(s[contador]))
		{
			return 1 << 31;
		}
		res *= 10;
		res += s[contador] - '0';
		contador++;
	}

	return (inicial != '-') ? res : -res;
}

