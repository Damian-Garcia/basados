/**
 * @file InterruptConfig.h
 * @brief This header defines the functions for interrupt enabling in the microcontroller
 */
#ifndef SOURCE_INTERRUPTCONFIG_H_
#define SOURCE_INTERRUPTCONFIG_H_

/**
 * @brief Enables all interrupts for the system
 */
void InterruptConfig_enableAllInterrupts();

/**
 * @brief Enables the UART interrupts for the system
 */
void InterruptConfig_enableUARTInterrupts();
/**
 * @brief Enables the GPIO interrupts for the system
 */
void InterruptConfig_enableGPIOInterrupts();

#endif /* SOURCE_INTERRUPTCONFIG_H_ */
