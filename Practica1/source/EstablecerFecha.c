/**
 * @file EstablecerFecha.c
 * @brief This is the source file for the Establecer Fecha header file
 */
#include "EstablecerFecha.h"

#include "stdbool.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "IPC.h"
#include "Mensajes.h"
#include "StringUtils.h"

bool writeFechaRTC(uint8_t *hora)
{

	Date t;

	t.d = (hora[0] & 0xf) << 4 | (hora[1] & 0xf);
	t.m = (hora[3] & 0xf) << 4 | (hora[4] & 0xf);
	t.a = (hora[8] & 0xf) << 4 | (hora[9] & 0xf);

	bool res = RTC7940_updateFecha(&t);

	return res;
}

bool EstablecerFecha_tryToUpdate(uint8_t * fecha, UART_Type *currentUart)
{

	bool res = writeFechaRTC(fecha);
	if (res)
	{
		imprimirMensajeExito(currentUart);
	}
	else
	{
		imprimirMensajeError(currentUart);
	}
	return res;
}

