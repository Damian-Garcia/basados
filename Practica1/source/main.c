/**
 * @file main.c
 * @brief Main file for the Practica1 projecto
 */

#include <string.h>

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
/*#include "fsl_debug_console.h"*/

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "ClockConfig.h"
#include "PortConfig.h"
#include "UARTConfig.h"

#include "InterruptConfig.h"
#include "MenuPrincipal.h"
#include "InputParser.h"
#include "EstablecerFecha.h"
#include "Mensajes.h"
#include "IPC.h"
#include "RTC.h"
#include "SPIConfig.h"
#include "LCDNokia5110.h"
#include "Pantalla.h"

volatile Cliente *c1;
volatile Cliente *c2;

int main(void)
{
	/*Init Board*/
	BOARD_BootClockRUN();
	ClockConfig_configAllClocks();
	PortConfig_configAllPins();
	UARTConfig_initUARTs();
	InterruptConfig_enableAllInterrupts();

	/*Init Mutual Exclusion + Messages*/
	IPC_init();

	/*Init external peripherals*/
	RTC7940_init();

	LCDNokia_init();

	c1 = pvPortMalloc(sizeof(Cliente));
	c2 = pvPortMalloc(sizeof(Cliente));

	c1->inputQueue = IPC_getInputParser1Queue();
	c1->menuQueue = IPC_getMenu1Queue();
	c1->uart = UARTConfig_getUARTCliente1();

	c2->inputQueue = IPC_getInputParser2Queue();
	c2->menuQueue = IPC_getMenu2Queue();
	c2->uart = UARTConfig_getUARTCliente2();

	/*Create Tasks*/
	xTaskCreate(InputParser_parser1task, "input parser",
			configMINIMAL_STACK_SIZE, (void *) c1, configMAX_PRIORITIES - 2,
			NULL);
	xTaskCreate(Menu_taskMenu, "menu 1", configMINIMAL_STACK_SIZE, (void *) c1,
			configMAX_PRIORITIES - 2, NULL);

	xTaskCreate(InputParser_parser2task, "InputPar2", configMINIMAL_STACK_SIZE,
			(void *) c2, configMAX_PRIORITIES - 2, NULL);
	xTaskCreate(Menu_taskMenu, "menu 2", configMINIMAL_STACK_SIZE, (void *) c2,
			configMAX_PRIORITIES - 2, NULL);

	xTaskCreate(PantallaUpdate_taks, "Pantalla", configMINIMAL_STACK_SIZE, NULL,
			configMAX_PRIORITIES - 2, NULL);


	vTaskStartScheduler();

	for (;;)
	{
		__asm("NOP");
	}

}
