/**
 * @file RTC.c
 * @brief This is the source file for the RTC header file
 */
#include "RTC.h"

#include "fsl_clock.h"
#include "I2CConfig.h"

static const uint8_t RTC7940_ADDR = 0x6F;
static const uint8_t RTC7940_SEC = 0x00;
static const uint8_t RTC7940_MIN = 0x01;
static const uint8_t RTC7940_HOR = 0x02;

static const uint8_t RTC7940_DAY = 0x04;
static const uint8_t RTC7940_MON = 0x05;
static const uint8_t RTC7940_YEAR = 0x06;

volatile uint8_t format;

static uint8_t convertBCDToUInt8(uint8_t n)
{
	uint8_t res = 0;
	res += (n >> 4) * 10 + (n & 0xF);
	return res;
}

void RTC7940_init()
{

	I2CConfig_init();
	I2CConfig_WriteReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_SEC, 0x80);
}

bool RTC7940_updateHora(Time *time)
{
	bool res = true;

	uint8_t mask = 0;
	uint8_t temp = convertBCDToUInt8(time->h);

	uint8_t s = time->s | 0x80;
	uint8_t m = time->m;
	uint8_t h = time->h | mask;

	res &= I2CConfig_WriteReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_SEC, s);
	res &= I2CConfig_WriteReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_MIN, m);
	res &= I2CConfig_WriteReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_HOR, h);
	return res;
}

bool RTC7940_updateFecha(Date *date)
{
	bool res = true;
	uint8_t d, m, a;
	res &= I2CConfig_WriteReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_DAY, date->d);
	res &= I2CConfig_WriteReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_MON, date->m);
	res &= I2CConfig_WriteReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_YEAR, date->a);
	return res;
}

bool RTC7940_readHora(Time *t)
{
	bool res = true;

	uint8_t s, m, h;

	res &= I2CConfig_ReadReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_HOR, &h, 1);
	res &= I2CConfig_ReadReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_MIN, &m, 1);
	res &= I2CConfig_ReadReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_SEC, &s, 1);

	t->s = convertBCDToUInt8(s & 0x7F);
	t->m = convertBCDToUInt8(m);
	t->h = convertBCDToUInt8(h & 0x3F);

	t->f = h >> 6;
	t->ampm = (h >> 5) & 0x1;

	if (t->f && t->ampm && t->h > 12)
	{
		t->h &= ~(1 << 5);
		t->h -= 12;
	}
	return res;
}

bool RTC7940_readFecha(Date *date)
{
	bool res = true;
	uint8_t d, m, a;

	res &= I2CConfig_ReadReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_DAY, &d, 1);
	res &= I2CConfig_ReadReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_MON, &m, 1);
	res &= I2CConfig_ReadReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_YEAR, &a, 1);

	date->a = convertBCDToUInt8(a);
	date->m = convertBCDToUInt8(m & 0x1F);
	date->d = convertBCDToUInt8(d & 0x3F);

	return res;
}

bool RTC7940_getFormato(uint8_t *f)
{
	bool res = true;
	res &= I2CConfig_ReadReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_HOR, f, 1);
	return res;
}

bool RTC7940_changeFormat(uint8_t f)
{
	uint8_t s, m, h;

	bool res = true;
	res &= I2CConfig_ReadReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_HOR, &h, 1);
	h ^= (1 << 6);
	res &= I2CConfig_WriteReg(RTC7940_ADDR, RTC7940_I2C, RTC7940_HOR, h);

	return res;
}
