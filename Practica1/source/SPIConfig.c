/**
 * @file SPIConfig.c
 * @brief This is the source file for the SPIConfig header file
 */
#include "SPIConfig.h"

#define EXAMPLE_DSPI_MASTER_BASEADDR SPI0
#define DSPI_MASTER_CLK_SRC DSPI0_CLK_SRC
#define EXAMPLE_DSPI_MASTER_PCS_FOR_INIT kDSPI_Pcs0
#define EXAMPLE_DSPI_MASTER_PCS_FOR_TRANSFER kDSPI_MasterPcs0

#define TRANSFER_SIZE 256U        /*! Transfer dataSize */
#define TRANSFER_BAUDRATE 500000U /*! Transfer baudrate - 500k */

void SPIConfig_init()
{
	uint32_t srcClock_Hz;

	dspi_master_config_t masterConfig;
	dspi_command_data_config_t commandConfig;

	DSPI_MasterGetDefaultConfig(&masterConfig);
	DSPI_GetDefaultDataCommandConfig(&commandConfig);

	srcClock_Hz = CLOCK_GetBusClkFreq();
	DSPI_MasterInit(EXAMPLE_DSPI_MASTER_BASEADDR, &masterConfig, srcClock_Hz);

}

