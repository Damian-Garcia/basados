/**
 * @file MenuPrincipal.c
 * @brief This is the source file for the MenuPrincipal header file
 */
#include "MenuPrincipal.h"

#include "stdbool.h"

#include "queue.h"
#include "semphr.h"

#include "IPC.h"
#include "StringUtils.h"
#include "Formatter.h"
#include "UARTConfig.h"
#include "Mensajes.h"
#include "EstablecerHora.h"
#include "EstablecerFecha.h"
#include "LeerHora.h"
#include "LeerMemoria.h"
#include "FormatoHora.h"
#include "EscribirMemoria.h"
#include "LeerFecha.h"
#include "Pantalla.h"

#define MAX_BUFFER_SIZE 1024

bool inChat1, inChat2;

void processChat(UART_Type * currentUart, QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;
	uint8_t bufferSalida[10];

	InputMessage mensajeEntrada;

	imprimirMensaje(currentUart, "Iniciando chat\r\n");

	if (currentUart == UART0)
	{
		inChat1 = true;
		if (!inChat2)
		{
			QueueHandle_t q = IPC_getMenu2Queue();
			InputMessage *im = pvPortMalloc(sizeof(InputMessage));
			im->mensaje = pvPortMalloc(2);
			im->mensaje[0] = '8';
			im->mensaje[1] = 0;
			im->codigo = INPUTMESSAGE_COMMAND;
			xQueueSend(q, im, 0);
		}
	}
	else
	{
		inChat2 = true;
		if (!inChat1)
		{
			QueueHandle_t q = IPC_getMenu1Queue();
			InputMessage *im = pvPortMalloc(sizeof(InputMessage));
			im->mensaje = pvPortMalloc(2);
			im->mensaje[0] = '8';
			im->mensaje[1] = 0;
			im->codigo = INPUTMESSAGE_COMMAND;
			xQueueSend(q, im, 0);
		}
	}
	imprimirMensaje(currentUart, "esperando chat\r\n");

	for (;;)
	{

		xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);

		if (mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
		{
			if (currentUart == UART0)
			{
				inChat1 = false;
				if (inChat2)
				{
					imprimirMensaje(UART4, "La terminal 1 se desconecto\r\n");
					QueueHandle_t q = IPC_getMenu2Queue();
					InputMessage *im = pvPortMalloc(sizeof(InputMessage));
					im->codigo = INPUTMESSAGE_ESCAPE;
					xQueueSend(q, im, 0);
				}
			}
			else
			{
				inChat2 = false;
				if (inChat1)
				{
					imprimirMensaje(UART0, "La terminal 2 se desconecto\r\n");
					QueueHandle_t q = IPC_getMenu1Queue();
					InputMessage *im = pvPortMalloc(sizeof(InputMessage));
					im->codigo = INPUTMESSAGE_ESCAPE;
					xQueueSend(q, im, 0);
				}
			}
			return;
		}

		if (currentUart == UART0)
		{
			imprimirMensaje(UART4, mensajeEntrada.mensaje);
		}
		else
		{
			imprimirMensaje(UART0, mensajeEntrada.mensaje);
		}
	}

}

void processECO_LCD(UART_Type * currentUart, QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;

	SemaphoreHandle_t taskMutex = IPC_getEcoLCDMutex();

	InputMessage mensajeEntrada;
	Pantalla_listen(true);
	QueueHandle_t pantallaQueue = IPC_getLCDQueue();

	BaseType_t tryToTake;
	tryToTake = xSemaphoreTake(taskMutex, 0);

	if (tryToTake == pdFAIL)
	{
		imprimirRecursoEnUso(currentUart);
		return;
	}

	for (;;)
	{
		xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);
		uint8_t c = 0;

		if (mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
		{
			Pantalla_listen(false);
			xQueueSend(pantallaQueue, &c, portMAX_DELAY);
			return;
		}
	}
}

void processEstablecerFecha(UART_Type * currentUart,
		QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;

	SemaphoreHandle_t taskMutex = IPC_getEstablecerFechaMutex();

	uint8_t bufferSalida[10];

	InputMessage mensajeEntrada;

	ProcessMessage respuestaRTC;
	ProcessMessage mensajeSalida;

	mensajeSalida.mensaje = bufferSalida;
	BaseType_t tryToTake;
	tryToTake = xSemaphoreTake(taskMutex, 0);

	if (tryToTake == pdFAIL)
	{
		imprimirRecursoEnUso(currentUart);
		return;
	}

	imprimirEstablecerFecha(currentUart);
	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);

	if (mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
	{
		xSemaphoreGive(taskMutex);
		return;
	}

	stringCopy(mensajeSalida.mensaje, mensajeEntrada.mensaje);

	EstablecerFecha_tryToUpdate(bufferSalida, currentUart);

	imprimirEsperarTecla(currentUart);

	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);
	xSemaphoreGive(taskMutex);

}

void processEstablecerHora(UART_Type * currentUart,
		QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;

	SemaphoreHandle_t taskMutex = IPC_getEstablecerHoraMutex();

	uint8_t bufferSalida[10];

	InputMessage mensajeEntrada;

	BaseType_t tryToTake;
	tryToTake = xSemaphoreTake(taskMutex, 0);

	if (tryToTake == pdFAIL)
	{
		imprimirRecursoEnUso(currentUart);
		return;
	}

	imprimirEstablecerHora(currentUart);
	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);

	if (mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
	{
		xSemaphoreGive(taskMutex);
		return;
	}

	stringCopy(bufferSalida, mensajeEntrada.mensaje);

	EstablecerHora_tryToUpdate(bufferSalida, currentUart);

	imprimirEsperarTecla(currentUart);

	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);
	xSemaphoreGive(taskMutex);

}

void processLeerHora(UART_Type * currentUart, QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;

	SemaphoreHandle_t taskMutex = IPC_getLeerHoraMutex();

	uint8_t bufferSalida[10];

	InputMessage mensajeEntrada;

	BaseType_t tryToTake;
	tryToTake = xSemaphoreTake(taskMutex, 0);

	if (tryToTake == pdFAIL)
	{
		imprimirRecursoEnUso(currentUart);
		return;
	}

	imprimirLeerHora(currentUart);

	LeerHora_tryToRead(currentUart);

	imprimirEsperarTecla(currentUart);

	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);
	xSemaphoreGive(taskMutex);

}

void processLeerMemoria(UART_Type * currentUart, QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;

	SemaphoreHandle_t taskMutex = IPC_getLeerMemoriaMutex();

	uint8_t bufferDireccion[10];
	uint8_t Contenido[20];

	InputMessage mensajeEntrada;

	BaseType_t tryToTake;
	tryToTake = xSemaphoreTake(taskMutex, 0);

	if (tryToTake == pdFAIL)
	{
		imprimirRecursoEnUso(currentUart);
		return;
	}

	imprimirLeerMemoriaDireccion(currentUart);
	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);

	if (mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
	{
		xSemaphoreGive(taskMutex);
		return;
	}
	stringCopy(bufferDireccion, mensajeEntrada.mensaje);

	imprimirLeerMemoriaContenido(currentUart);
	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);

	if (mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
	{
		xSemaphoreGive(taskMutex);
		return;
	}

	stringCopy(Contenido, mensajeEntrada.mensaje);

	LeerMemoria_tryToRead(bufferDireccion, Contenido, currentUart);

	imprimirEsperarTecla(currentUart);

	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);
	xSemaphoreGive(taskMutex);

}

void processFormatoHora(UART_Type* currentUart, QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;

	SemaphoreHandle_t taskMutex = IPC_getFormatoHoraMutex();

	uint8_t bufferSalida[10];

	InputMessage mensajeEntrada;

	BaseType_t tryToTake;
	tryToTake = xSemaphoreTake(taskMutex, 0);

	if (tryToTake == pdFAIL)
	{
		imprimirRecursoEnUso(currentUart);
		return;
	}

#if 0
	if(mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
	{
		xSemaphoreGive(taskMutex);
		return;
	}
#endif

	stringCopy(bufferSalida, mensajeEntrada.mensaje);

	FormatoHora_tryToUpdate(bufferSalida, currentUart);

	imprimirEsperarTecla(currentUart);

	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);
	xSemaphoreGive(taskMutex);

}

void processEscribirMemoria(UART_Type * currentUart,
		QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;

	SemaphoreHandle_t taskMutex = IPC_getEscribirMemoriaMutex();

	uint8_t bufferDireccion[10];
	uint8_t Contenido[20];

	InputMessage mensajeEntrada;

	BaseType_t tryToTake;
	tryToTake = xSemaphoreTake(taskMutex, 0);

	if (tryToTake == pdFAIL)
	{
		imprimirRecursoEnUso(currentUart);
		return;
	}

	imprimirEscribirMemoriaDireccion(currentUart);
	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);

	if (mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
	{
		xSemaphoreGive(taskMutex);
		return;
	}
	stringCopy(bufferDireccion, mensajeEntrada.mensaje);

	imprimirEscribirMemoriaContenido(currentUart);
	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);

	if (mensajeEntrada.codigo != INPUTMESSAGE_COMMAND)
	{
		xSemaphoreGive(taskMutex);
		return;
	}

	stringCopy(Contenido, mensajeEntrada.mensaje);

	EscribirMemoria_tryToWrite(bufferDireccion, Contenido, currentUart);

	imprimirEsperarTecla(currentUart);

	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);
	xSemaphoreGive(taskMutex);

}

void processLeerFecha(UART_Type * currentUart, QueueHandle_t currentMenuQueue)
{
	QueueHandle_t entradaQueue = currentMenuQueue;

	SemaphoreHandle_t taskMutex = IPC_getLeerFechaMutex();

	uint8_t bufferSalida[10];

	InputMessage mensajeEntrada;

	BaseType_t tryToTake;
	tryToTake = xSemaphoreTake(taskMutex, 0);

	if (tryToTake == pdFAIL)
	{
		imprimirRecursoEnUso(currentUart);
		return;
	}

	imprimirLeerFecha(currentUart);

	LeerFecha_tryToRead(currentUart);

	imprimirEsperarTecla(currentUart);

	xQueueReceive(entradaQueue, &mensajeEntrada, portMAX_DELAY);
	xSemaphoreGive(taskMutex);
}

PRACTICA_TASKS processInput(InputMessage *m)
{
	if (m == NULL)
	{
		return TASK_INVALID;
	}

	uint8_t command[30] =
	{ };
	stringCopy(command, m->mensaje);

	int32_t parsedCommand = parseASCIIAsInt32(command);

	return convertToTask(parsedCommand);
}

void processSelectedTask(PRACTICA_TASKS selectedTask, UART_Type * currentUart,
		QueueHandle_t currenMenuQueue)
{
	switch (selectedTask)
	{
	case TASK_CHAT:
		processChat(currentUart, currenMenuQueue);
		break;

	case TASK_ECO_LCD:
		processECO_LCD(currentUart, currenMenuQueue);
		break;

	case TASK_ESTABLECER_FECHA:
		processEstablecerFecha(currentUart, currenMenuQueue);
		break;

	case TASK_ESTABLECER_HORA:
		processEstablecerHora(currentUart, currenMenuQueue);
		break;

	case TASK_FORMATO_HORA:
		processFormatoHora(currentUart, currenMenuQueue);
		break;

	case TASK_LEER_HORA:
		processLeerHora(currentUart, currenMenuQueue);
		break;

	case TASK_LEER_MEMORIA:
		processLeerMemoria(currentUart, currenMenuQueue);
		break;

	case TASK_ESCRIBIR_MEMORIA:
		processEscribirMemoria(currentUart, currenMenuQueue);
		break;

	case TASK_LEER_FECHA:
		processLeerFecha(currentUart, currenMenuQueue);
		break;

	default:
		break;
	}
}

void Menu_taskMenu(void *p)
{
	void initMenu();
	bool opcionInvalida = false;

	Cliente *c = (Cliente *) p;

	QueueHandle_t menuQueue = c->menuQueue;
	InputMessage mensaje;

	PRACTICA_TASKS opcion = TASK_IDLE;

	UART_Type * currentUart = c->uart;

	for (;;)
	{
		if (opcion == TASK_INVALID)
		{
			imprimirOpcionInvalida(currentUart);
		}

		opcion = TASK_IDLE;
		imprimirMenu(currentUart);
		xQueueReceive(menuQueue, &mensaje, portMAX_DELAY);

		if (mensaje.codigo == INPUTMESSAGE_ESCAPE)
		{
			continue;
		}

		opcion = processInput(&mensaje);

		processSelectedTask(opcion, currentUart, menuQueue);
	}

}

