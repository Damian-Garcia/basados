/**
 * @file EscribirMemoria.h
 * @brief This is the header file for the external i2c memory writing
 */
#ifndef SOURCE_ESCRIBIRMEMORIA_H_
#define SOURCE_ESCRIBIRMEMORIA_H_

#include "stdbool.h"
#include "fsl_uart.h"
#include "FreeRTOS.h"

/**
 * @brief This function writes data to external i2c memory
 * @param dir an ASCII representation of the address to write
 * @param cont Bytes to write
 * @param currentUart UART to print result of the operation
 */
bool EscribirMemoria_tryToWrite(uint8_t *dir, uint8_t *cont,
		UART_Type * currentUart);

#endif /* SOURCE_ESCRIBIRMEMORIA_H_ */
