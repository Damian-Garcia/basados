/**
 * @file IPC.h
 * @brief This header defines an API for communication between tasks
 */
#ifndef SOURCE_IPC_H_
#define SOURCE_IPC_H_

#include "fsl_uart.h"

#include "FreeRTOS.h"

#include "queue.h"
#include "semphr.h"
#include "event_groups.h"

/**
 * @brief Enum for the different types of chars it can be received via uart
 *
 */
typedef enum
{
	INPUTCHAR_CHAR, INPUTCHAR_ESC, INPUTCHAR_ENTER
} CodigoInputChar;

/**
 * @brief Enum for the different types of messages the menu can receive
 */
typedef enum
{
	INPUTMESSAGE_COMMAND, INPUTMESSAGE_ESCAPE,
} CodigoMensajeEntrada;

/**
 * @brief Enum for the diferrent type of messages a task can send to another
 */
typedef enum
{
	MENSAJE_ERROR, MENSAJE_DATA, MENSAJE_EXITO, MENSAJE_REINICIAR, MENSAJE_SALIR
} CodigoMensajeProceso;

/**
 * @brief An enum of the different submenus that can be executed
 */
typedef enum
{
	TASK_INVALID,
	TASK_LEER_MEMORIA,
	TASK_ESCRIBIR_MEMORIA,
	TASK_ESTABLECER_HORA,
	TASK_ESTABLECER_FECHA,
	TASK_FORMATO_HORA,
	TASK_LEER_HORA,
	TASK_LEER_FECHA,
	TASK_CHAT,
	TASK_ECO_LCD,
	TASK_IDLE,
	TASK_NUMBEROFTASKS
} PRACTICA_TASKS;

/**
 * @brief Type structure for passing messages between  UART and Input Parser
 */
typedef struct
{
	CodigoMensajeEntrada codigo;
	uint8_t *mensaje;
} InputMessage;


/**
 * @brief Type structure for passing messages between processes
 */
typedef struct
{
	CodigoMensajeProceso codigo;
	uint8_t * mensaje;
} ProcessMessage;

/**
 * @brief Type structure that defines an UART client
 */
typedef struct
{
	QueueHandle_t inputQueue;
	QueueHandle_t menuQueue;
	UART_Type * uart;
} Cliente;

/**
 * @brief Initialize inter process communication variables
 * This function calls the operating system functions that initializes
 * mutexes and queues
 */
void IPC_init();

/**
 * @brief function to securily convert a uint32 to a task object.
 * @return PRACTICA_TASK object equivalent of the input
 */
PRACTICA_TASKS convertToTask(uint32_t task);

/**
 * @brief Access Method to the Input Parser 1 Queue
 * @return QueueHandle_t value
 */
QueueHandle_t IPC_getInputParser1Queue();
/**
 * @brief Access Method to the Input Parser 2 Queue
 * @return QueueHandle_t value
 */
QueueHandle_t IPC_getInputParser2Queue();
/**
 * @brief Access Method to the Menu 1 Queue
 * @return QueueHandle_t value
 */
QueueHandle_t IPC_getMenu1Queue();
/**
 * @brief Access Method to the Menu 2 Queue
 * @return QueueHandle_t value
 */
QueueHandle_t IPC_getMenu2Queue();
/**
 * @brief Access Method to the LCD Process queue
 * @return QueueHandle_t value
 */
QueueHandle_t IPC_getLCDQueue();
/**
 * @brief Acces Method to the Mutex of the LCD Eco function
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getEcoLCDMutex();
/**
 * @brief Acces Method to the Mutex of the Write Memory function
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getEscribirMemoriaMutex();
/**
 * @brief Acces Method to the Mutex of the Write time function
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getEstablecerHoraMutex();
/**
 * @brief Acces Method to the Mutex of the Write date function
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getEstablecerFechaMutex();
/**
 * @brief Acces Method to the Mutex of the change format function
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getFormatoHoraMutex();
/**
 * @brief Acces Method to the Mutex of the Read time function
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getLeerHoraMutex();
/**
 * @brief Acces Method to the Mutex of the Read date function
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getLeerFechaMutex();
/**
 * @brief Acces Method to the Mutex of the write memory
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getLeerMemoriaMutex();
/**
 * @brief Acces Method to the Mutex of the local LCD printing function
 * @return SemaphoreHandle_t containing the mutex
 */
SemaphoreHandle_t IPC_getPantallaMutex();

#endif /* SOURCE_IPC_H_ */
