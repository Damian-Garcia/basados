/**
 * @file SPIConfig.h
 * @brief This header defines an API to configure and use the SPI communication module
 */
#ifndef SOURCE_SPICONFIG_H_
#define SOURCE_SPICONFIG_H_

#include "fsl_dspi.h"
#include "stdbool.h"

/**
 * @brief This function configures SPI communication module
 */
void SPIConfig_init();
/**
 * @brief Write a byte to an SPI device
 * @param SPI as the base of the microcontroller SPI device
 * @param masterTxData Data to transmit
 * @param masterRxData Buffer to store the Read Data
 */
void SPIConfig_sendByte(SPI_Type* SPI, uint8_t * masterTxData,
		uint8_t *masterRxData);

#endif /* SOURCE_SPICONFIG_H_ */
