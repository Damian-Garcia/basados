/**
 * @file InterruptConfig.c
 * @brief This is the source file for the InterruptConfig header file
 */
#include "InterruptConfig.h"

#include "UARTConfig.h"

void InterruptConfig_enableUARTInterrupts()
{
	NVIC_SetPriority(UART0_RX_TX_IRQn, 5);
	NVIC_SetPriority(UART4_RX_TX_IRQn, 5);

	NVIC_EnableIRQ(UART0_RX_TX_IRQn);
	NVIC_EnableIRQ(UART4_RX_TX_IRQn);
}

void InterruptConfig_enableGPIOInterrupts()
{
	NVIC_SetPriority(PORTC_IRQn, 5);
	NVIC_EnableIRQ(PORTC_IRQn);
}

void InterruptConfig_enableAllInterrupts()
{
	InterruptConfig_enableUARTInterrupts();
	InterruptConfig_enableGPIOInterrupts();
}
