/**
 * @file EstablecerHora.h
 * @brief This header defines the functions that update the time in the RCTC
 */

#ifndef SOURCE_ESTABLECERHORA_H_
#define SOURCE_ESTABLECERHORA_H_

#include "stdbool.h"
#include "fsl_uart.h"
#include "FreeRTOS.h"

/**
 * @brief This function tries to update the time in the RTC
 * @param s ASCII string with the time in the format hh:mm:ss
 * @param uart UART to print the results
 * @return true if it was possible to write, false otherwise
 */
bool EstablecerHora_tryToUpdate(uint8_t * s, UART_Type* uart);

#endif /* SOURCE_ESTABLECERHORA_H_ */
