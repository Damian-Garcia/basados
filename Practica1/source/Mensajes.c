/**
 * @file Mensajes.c
 * @brief This is the source file for the Mensajes header file
 */
#include "Mensajes.h"
#include "StringUtils.h"
#include "RTC.h"
#include "Formatter.h"

uint8_t * mm_mensajeError = "error\r\n";
uint8_t * mm_mensajeExito = "exito\r\n";
uint8_t * mm_menu =
		"menu\r\n1: Leer Memoria\r\n2: EscribirMemoria\r\n3: EstablecerHora\r\n4: Establecer Fecha\r\n"
				"5: Formato Hora\r\n6: Leer Hora\r\n7: Leer Fecha\r\n8: Chat\r\n9: Eco\r\n";
uint8_t * mm_mensajeOpcionInvalida = "opcion invalida\r\n";
uint8_t * mm_mensajeperdirFecha = "pedirFecha \r\n";
uint8_t * mm_mensajeRecursoEnUso = "recurso en uso \r\n";
uint8_t * mm_mensajeEstablecerFecha = "establecer fecha\r\n";
uint8_t * mm_mensajeEsperarTecla = "Presiona Enter";
uint8_t * mm_mensajeEstablecerHora = "Establecer Hora\r\n";
uint8_t * mm_mensajeLeerHora = "La hora actual es \r\n";
uint8_t * mm_mensajeleerMemoriaDireccion = "Escribe la direccion a leer\r\n";
uint8_t * mm_mensajeleerMemoriaContenido = "Escribe la cantidad de bytes a leer";
uint8_t * mm_mensajeFormatoHora = "Ingresa am/pm o 24 horas";
uint8_t * mm_mensajeescribirMemoriaDireccion =
		"Escribe la direccion a escribir\r\n";
uint8_t * mm_mensajeescribirMemoriaContenido =
		"Escribe los datos a la memoria\r\n";
uint8_t * mm_mensajeleerFecha = "La fecha actual es:\r\n";

void imprimirMensaje(UART_Type *uart, uint8_t * mensaje)
{
	uint32_t len = stringLength(mensaje);
	UART_WriteBlocking(uart, mensaje, len);
}

void imprimirMensajeError(UART_Type* uart)
{
	imprimirMensaje(uart, mm_mensajeError);
}

void imprimirMensajeExito(UART_Type* uart)
{
	imprimirMensaje(uart, mm_mensajeExito);
}

void imprimirMenu(UART_Type* uart)
{
	imprimirMensaje(uart, mm_menu);
}

void imprimirOpcionInvalida(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeOpcionInvalida);
}

void imprimirPedirFecha(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeperdirFecha);
}

void imprimirRecursoEnUso(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeRecursoEnUso);
}

void imprimirEstablecerFecha(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeEstablecerFecha);
}

void imprimirEsperarTecla(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeEsperarTecla);
}

void imprimirEstablecerHora(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeEstablecerHora);
}

void imprimirLeerHora(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeLeerHora);
}

void imprimirLeerMemoriaDireccion(UART_Type * uart)
{
	imprimirMensaje(uart, mm_mensajeleerMemoriaDireccion);
}

void imprimirLeerMemoriaContenido(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeleerMemoriaContenido);
}
void imprimirFormatoHora(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeFormatoHora);
}

void imprimirEscribirMemoriaDireccion(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeescribirMemoriaDireccion);
}

void imprimirEscribirMemoriaContenido(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeescribirMemoriaContenido);
}

void imprimirLeerFecha(UART_Type*uart)
{
	imprimirMensaje(uart, mm_mensajeleerFecha);
}

void imprimirTiempo(UART_Type *uart, Time *t)
{
	uint8_t hB[5], sB[5], mB[5];
	formatUnsignedInteger32(t->h, hB, 2);
	formatUnsignedInteger32(t->m, mB, 2);
	formatUnsignedInteger32(t->s, sB, 2);

	imprimirMensaje(uart, "Hora: ");
	imprimirMensaje(uart, hB);
	imprimirMensaje(uart, ":");
	imprimirMensaje(uart, mB);
	imprimirMensaje(uart, ":");
	imprimirMensaje(uart, sB);
	imprimirMensaje(uart, " ");
	if (t->f && !(t->ampm))
	{
		imprimirMensaje(uart, "am");
	}
	if (t->f && (t->ampm))
	{
		imprimirMensaje(uart, "pm");
	}
	imprimirMensaje(uart, "\r\n");
}

void imprimirFecha(UART_Type *uart, Date *t)
{
	uint8_t aB[5], mB[5], dB[5];
	formatUnsignedInteger32(t->a, aB, 2);
	formatUnsignedInteger32(t->m, mB, 2);
	formatUnsignedInteger32(t->d, dB, 2);

	imprimirMensaje(uart, "Fecha : ");
	imprimirMensaje(uart, dB);
	imprimirMensaje(uart, "/");
	imprimirMensaje(uart, mB);
	imprimirMensaje(uart, "/20");
	imprimirMensaje(uart, aB);
	imprimirMensaje(uart, "\r\n");

}
