/**
 * @file RTC.h
 * @brief This header defines an API to interface the external RTC
 */
#ifndef SOURCE_RTC_H_
#define SOURCE_RTC_H_

#include "fsl_i2c.h"
#include "FreeRTOS.h"

#define RTC7940_I2C I2C0

#define RTC7940_FORMATO_AMPM 0
#define RTC7940_FORMATO_24 1

/**
 * @brief Type structure to represent time
 */
typedef struct
{
	uint8_t h, m, s;
	uint8_t *hB, *mB, *sB;
	uint8_t ampm;
	uint8_t f;
} Time;
/**
 * @brief Type structure to represent date
 */
typedef struct
{
	uint8_t d, m, a;
	uint8_t f;
} Date;

/**
 * @brief Util function to convert a byte to its BCD representation
 */
static uint8_t convertBCDToUInt8(uint8_t n);
/**
 * @brief Initialization method for the RTC.
 * This method enables the I2C communication and starts the time
 * counting.
 */
void RTC7940_init();

/**
 * @brief Update RTC time interface method
 * @param time The new time to write
 * @return true if update operation was successful
 */
bool RTC7940_updateHora(Time *time);
/**
 * @brief Update RTC date interface method
 * @param date The new time to write
 * @return true if update operation was successful
 */
bool RTC7940_updateFecha(Date *date);
/**
 * @brief Read RTC time interface method
 * @param t A time buffer to write the read time
 * @return true if read operation was successful, false otherwise
 */
bool RTC7940_readHora(Time *t);
/**
 * @brief Read RTC date interface method
 * @param d A date buffer to write the read date.
 * @return true if read operation was successful, false otherwise
 */
bool RTC7940_readFecha(Date *d);
/**
 * @brief Read RTC format interface method
 * @param f buffer to store the format
 * @return true if read operation was successful, false otherwise
 */
bool RTC7940_getFormato(uint8_t *f);
/**
 * @brief Write RTC format interface method
 */
bool RTC7940_changeFormat(uint8_t f);

#endif /* SOURCE_RTC_H_ */
