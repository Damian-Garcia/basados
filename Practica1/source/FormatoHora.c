/**
 * @file FormatoHora.c
 * @brief This is the source file for the FormatoHora header file
 */
#include "FormatoHora.h"
#include "Mensajes.h"

bool formatoRTC(uint8_t *s)
{
	return RTC7940_changeFormat(0);
}

bool FormatoHora_tryToUpdate(uint8_t *s, UART_Type *currentUart)
{
	bool res = formatoRTC(s);
	if (res)
	{
		imprimirMensaje(currentUart, "Formato Cambiado");
	}
	else
	{
		imprimirMensajeError(currentUart);
	}
	return res;
}
