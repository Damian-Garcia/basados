/**
 * @file ClockConfig.h
 * @brief This header defines functions to prompt messages on an UART terminal
 */
#ifndef SOURCE_MENSAJES_H_
#define SOURCE_MENSAJES_H_

#include "FreeRTOS.h"
#include "fsl_uart.h"
#include "RTC.h"

/**
 * @brief Generic print to UART method
 * @param uart selected UART to print the message
 * @param mensaje as the buffer containing the message
 */
void imprimirMensaje(UART_Type*uart, uint8_t *mensaje);
/**
 * @brief prints an error message
 * @param uart as the selected UART
 */
void imprimirMensajeError(UART_Type*uart);
/**
 * @brief prints a success  message
 * @param uart as the selected UART
 */
void imprimirMensajeExito(UART_Type*uart);
/**
 * @brief prints the menu
 * @param uart as the selected UART
 */
void imprimirMenu(UART_Type*uart);
/**
 * @brief prints the invalid option message
 * @param uart as the selected UART
 */
void imprimirOpcionInvalida(UART_Type*uart);
/**
 * @brief prints a Write date submenu
 * @param uart as the selected UART
 */
void imprimirPedirFecha(UART_Type*uart);
/**
 * @brief prints a busy resource message
 * @param uart as the selected UART
 */
void imprimirRecursoEnUso(UART_Type*uart);
/**
 * @brief prints the Write Date submenu
 * @param uart as the selected UART
 */
void imprimirEstablecerFecha(UART_Type*uart);
/**
 * @brief prints a type key message
 * @param uart as the selected UART
 */
void imprimirEsperarTecla(UART_Type*uart);
/**
 * @brief prints the Write time submenu
 * @param uart as the selected UART
 */
void imprimirEstablecerHora(UART_Type*uart);
/**
 * @brief prints the read time message
 * @param uart as the selected UART
 */
void imprimirLeerHora(UART_Type*uart);
/**
 * @brief prints the type address to read message
 * @param uart as the selected UART
 */
void imprimirLeerMemoriaDireccion(UART_Type*uart);
/**
 * @brief prints the type number of bytes to read message
 * @param uart as the selected UART
 */
void imprimirLeerMemoriaContenido(UART_Type*uart);
/**
 * @brief prints the change format message
 * @param uart as the selected UART
 */
void imprimirFormatoHora(UART_Type*uart);
/**
 * @brief prints the type address to write message
 * @param uart as the selected UART
 */
void imprimirEscribirMemoriaDireccion(UART_Type*uart);
/**
 * @brief prints the type content to write message
 * @param uart as the selected UART
 */
void imprimirEscribirMemoriaContenido(UART_Type*uart);
/**
 * @brief prints the read fecha message
 * @param uart as the selected UART
 */
void imprimirLeerFecha(UART_Type*uart);
/**
 * @brief prints a time to the uart
 * @param uart as the selected UART
 * @param t as the time type to write
 */
void imprimirTiempo(UART_Type *uart, Time *t);
/**
 * @brief prints a date to the uart
 * @param uart as the selected UART
 * @param t as the date type to write
 */
void imprimirFecha(UART_Type *uart, Date *t);

#endif /* SOURCE_MENSAJES_H_ */
