/**
 * @file ClockConfig.c
 * @brief This is the source file for the ClockConfig header file
 */
#include "fsl_clock.h"

#include "ClockConfig.h"

void ClockConfig_configGPIOClock()
{
	CLOCK_EnableClock(kCLOCK_PortA);
	CLOCK_EnableClock(kCLOCK_PortB);
	CLOCK_EnableClock(kCLOCK_PortC);
	CLOCK_EnableClock(kCLOCK_PortD);
	CLOCK_EnableClock(kCLOCK_PortE);
}

void ClockConfig_configUARTClock()
{
	CLOCK_EnableClock(kCLOCK_PortA);
	CLOCK_EnableClock(kCLOCK_PortE);
}

void ClockConfig_configI2CClock()
{
	CLOCK_EnableClock(kCLOCK_PortE);
}

void ClockConfig_configSPIClock()
{
	CLOCK_EnableClock(kCLOCK_PortD);
}

void ClockConfig_configAllClocks()
{
	ClockConfig_configGPIOClock();
	ClockConfig_configI2CClock();
	ClockConfig_configSPIClock();
	ClockConfig_configUARTClock();
}
