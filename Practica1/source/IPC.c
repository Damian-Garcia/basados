/**
 * @file IPC.c
 * @brief This is the source file for the IPC header file
 */
#include "IPC.h"

 volatile EventGroupHandle_t eventUART0;

 volatile QueueHandle_t inputParser1Queue;
 volatile QueueHandle_t inputParser2Queue;

 volatile QueueHandle_t menu1Queue;
 volatile QueueHandle_t menu2Queue;

 volatile QueueHandle_t chatQueue;
 volatile QueueHandle_t LCDQueue;

 SemaphoreHandle_t ecoLCDMutex;
 SemaphoreHandle_t escribirMemoriaMutex;
 SemaphoreHandle_t establecerHoraMutex;
 SemaphoreHandle_t establecerFechaMutex;
 SemaphoreHandle_t formatoHoraMutex;
 SemaphoreHandle_t leerHoraMutex;
 SemaphoreHandle_t leerFechaMutex;
 SemaphoreHandle_t leerMemoriaMutex;

 SemaphoreHandle_t pantallaLCDMutex;

void initQueues(){
	inputParser1Queue = xQueueCreate(15, sizeof (uint8_t *));
	inputParser2Queue = xQueueCreate(15, sizeof (uint8_t *));

	menu1Queue = xQueueCreate(5, sizeof (InputMessage));
	menu2Queue = xQueueCreate(5, sizeof (InputMessage));

	LCDQueue = xQueueCreate(10,sizeof(uint8_t));
}

void initMutexes(){
	ecoLCDMutex = xSemaphoreCreateMutex();
	escribirMemoriaMutex = xSemaphoreCreateMutex();
	establecerHoraMutex = xSemaphoreCreateMutex();
	establecerFechaMutex = xSemaphoreCreateMutex();
	formatoHoraMutex = xSemaphoreCreateMutex();
	leerHoraMutex = xSemaphoreCreateMutex();
	leerFechaMutex = xSemaphoreCreateMutex();
	leerMemoriaMutex = xSemaphoreCreateMutex();

	pantallaLCDMutex = xSemaphoreCreateMutex();
}

void IPC_init(){
	eventUART0 = xEventGroupCreate();
	initQueues();
	initMutexes();
}

PRACTICA_TASKS convertToTask(uint32_t task){
	if(task >= TASK_NUMBEROFTASKS){
		return TASK_INVALID;
	}
	return task;
}

EventGroupHandle_t IPC_getEventUart0(){
	return eventUART0;
}

QueueHandle_t IPC_getInputParser1Queue(){
	return inputParser1Queue;
}

QueueHandle_t IPC_getInputParser2Queue(){
	return inputParser2Queue;
}

QueueHandle_t IPC_getMenu1Queue(){
	return menu1Queue;
}

QueueHandle_t IPC_getMenu2Queue(){
	return menu2Queue;
}

QueueHandle_t IPC_getChatQueue(){
	return chatQueue;
}

QueueHandle_t IPC_getLCDQueue(){
	return LCDQueue;
}

SemaphoreHandle_t IPC_getEcoLCDMutex(){
	return ecoLCDMutex;
}
SemaphoreHandle_t IPC_getEscribirMemoriaMutex(){
	return escribirMemoriaMutex;
}

SemaphoreHandle_t IPC_getEstablecerHoraMutex(){
	return establecerHoraMutex;
}

SemaphoreHandle_t IPC_getEstablecerFechaMutex(){
	return establecerFechaMutex;
}

SemaphoreHandle_t IPC_getFormatoHoraMutex(){
	return formatoHoraMutex;
}

SemaphoreHandle_t IPC_getLeerHoraMutex(){
	return leerHoraMutex;
}

SemaphoreHandle_t IPC_getLeerFechaMutex(){
	return leerFechaMutex;
}

SemaphoreHandle_t IPC_getLeerMemoriaMutex(){
	return leerMemoriaMutex;
}

SemaphoreHandle_t IPC_getPantallaMutex(){
	return pantallaLCDMutex;
}

