/**
 * @file Memoria.c
 * @brief This is the source file for the Memoria header file
 */

#include "Memoria.h"
#include "StringUtils.h"

#define MEMORIA_I2C_ADDRESS 0x50
#define MEMORIA_I2C_BASE I2C0

#include "I2CConfig.h"

bool Memoria_leer(uint32_t direccion, uint32_t cantidadDeBytes,
		uint8_t * buffer)
{
	return I2CConfig_ReadBuffer(MEMORIA_I2C_ADDRESS, MEMORIA_I2C_BASE,
			direccion, buffer, 1, cantidadDeBytes);
}

bool Memoria_escribir(uint32_t direccion, uint32_t cantidadDeBytes,
		uint8_t *buffer)
{
	uint32_t len = stringLength(buffer);
	return I2CConfig_WriteBuffer(MEMORIA_I2C_ADDRESS, MEMORIA_I2C_BASE,
			direccion, buffer, len);
}

