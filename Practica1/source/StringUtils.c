/**
 * @file StringUtils.c
 * @brief This is the source file for the StringUtils header file
 */
#include "FreeRTOS.h"

int32_t stringCompare(const uint8_t * s1, const uint8_t * s2)
{
	uint32_t index;
	int32_t res = 0;
	while (*s1 && *s2)
	{
		res += s1[index] - s2[index];
		index++;
	}

	if (s1[index])
	{
		return res - s1[index];
	}

	if (s2[index])
	{
		return res - s2[index];
	}

	return res;
}

int32_t stringLength(const uint8_t *s)
{
	uint32_t contador = 0;
	while (s[contador])
	{
		contador++;
	}
	return contador;
}

void stringCopy(uint8_t * dest, uint8_t * src)
{
	uint32_t contador = 0;
	while (src[contador])
	{
		dest[contador] = src[contador];
		contador++;
	}
	dest[contador] = 0;
}

uint16_t getDigit(uint8_t c)
{
	return c - '0';
}
