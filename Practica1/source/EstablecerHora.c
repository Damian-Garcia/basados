/**
 * @file EstablecerHora.c
 * @brief This is the source file for the EstablecerHora header file
 */
#include "EstablecerHora.h"

#include "Mensajes.h"
#include "RTC.h"

bool writeHoraRTC(uint8_t *hora)
{

	Time t;

	t.h = (hora[0] & 0xf) << 4 | (hora[1] & 0xf);
	t.m = (hora[3] & 0xf) << 4 | (hora[4] & 0xf);
	t.s = (hora[6] & 0xf) << 4 | (hora[7] & 0xf);

	bool res = RTC7940_updateHora(&t);

	return res;
}

bool EstablecerHora_tryToUpdate(uint8_t * hora, UART_Type *currentUart)
{

	bool res = writeHoraRTC(hora);
	if (res)
	{
		imprimirMensajeExito(currentUart);
	}
	else
	{
		imprimirMensajeError(currentUart);
	}
}

