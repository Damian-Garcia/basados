/**
 * @file PortConfig.h
 * @brief This header defines the functions for port configuration in the microcontroller
 */
#ifndef SOURCE_PORTCONFIG_H_
#define SOURCE_PORTCONFIG_H_

#include "FreeRTOS.h"

/**
 * @brief This function configures the Kinetis PORTs to its desired function
 */
void PortConfig_configAllPins();
/**
 * This function configures the UART pins
 */
void PortConfig_configUARTPins();
/**
 * This function configures the UART pins
 */
void PortConfig_configI2CPins();
/**
 * This function configures the I2C pins
 */
void PortConfig_configSPIPins();
/**
 * This function configures the GPIO pins
 */
void PortConfig_configGPIOPins();

#endif /* SOURCE_PORTCONFIG_H_ */
