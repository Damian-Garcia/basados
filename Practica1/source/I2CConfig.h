/**
 * @file I2CConfig.h
 * @brief This header defines the functions for I2C configuration in the microcontroller
 */
#ifndef SOURCE_I2CCONFIG_H_
#define SOURCE_I2CCONFIG_H_

#include "stdbool.h"
#include "fsl_i2c.h"

/**
 * @brief Configures the i2c module in the board
 */
void I2CConfig_init();
/**
 * @brief This function tries to write in an address of a i2c device
 * @param slaveAdd as uint8_t representing the i2c slave address
 * @param I2C_base as I2C peripheral to use
 * @param reg_addr as subaddress to write
 * @param value as byte to write in the register
 * @return true if it was possible to write, false otherwise
 */
bool I2CConfig_WriteReg(uint8_t slaveAdd, I2C_Type* I2C_base, uint32_t reg_addr,
		uint8_t value);
/**
 * @brief This function tries to read in an address of a i2c device
 * @param slaveAdd as uint8_t representing the i2c slave address
 * @param I2C_base as I2C peripheral to use
 * @param reg_addr as subaddress to read
 * @param rxBuff as  byte buffer to place the result
 * @return true if it was possible to read, false otherwise
 */
bool I2CConfig_ReadReg(uint8_t slaveAdd, I2C_Type* I2C_base, uint32_t reg_addr,
		uint8_t *rxBuff, uint32_t rxSize);
/**
 * @brief This function tries to write a buffer to the i2c memory
 * @param slaveAdd as uint8_t representing the i2c slave address
 * @param I2C_base as I2C peripheral to use
 * @param begin_addr as subaddress to write
 * @param value as  ascii buffer to write in the memory
 * @param rxSize Number of bytes to read
 * @return true if it was possible to write the whole contents of the buffer
 */
bool I2CConfig_WriteBuffer(uint8_t slaveAdd, I2C_Type* I2C_base,
		uint16_t begin_addr, uint8_t* value, uint32_t len);
/**
 * @brief This function tries to read multiple locations of the i2c memory
 * @param slaveAdd as uint8_t representing the i2c slave address
 * @param I2C_base as I2C peripheral to use
 * @param begin_addr as subaddress to write
 * @param rxBuff as  ascii buffer to write in the memory
 * @param rxSize as size of an element in bytes
 * @param len The number of elements to write
 * @return true if it was possible to write the whole contents of the buffer
 */
bool I2CConfig_ReadBuffer(uint8_t slaveAdd, I2C_Type* I2C_base,
		uint16_t begin_addr, uint8_t *rxBuff, uint32_t rxSize, uint32_t len);

#endif /* SOURCE_I2CCONFIG_H_ */
