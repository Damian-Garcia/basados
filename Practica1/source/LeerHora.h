/**
 * @file LeerHora.h
 * @brief This header defines the functions for reading the time in the RTC
 */
#ifndef SOURCE_LEERHORA_H_
#define SOURCE_LEERHORA_H_

#include "fsl_uart.h"
/**
 * @brief This function tries to read the time from the RTC
 * The function will try to communicate with the RTC via i2c
 * If the communication fails, the function will return false.
 * If communication was successful, it will print a success
 * message in the selected UART.
 * @param currentUart as UART to write the message
 * @return true if write operation was successful, false otherwise
 */
bool LeerHora_tryToRead(UART_Type *currentUart);

#endif /* SOURCE_LEERHORA_H_ */
