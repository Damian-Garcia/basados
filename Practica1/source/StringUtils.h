/**
 * @file StringUtils.h
 * @brief This header defines the string util functions
 */
#ifndef SOURCE_STRINGUTILS_H_
#define SOURCE_STRINGUTILS_H_

/**
 * @brief Compares if two strings are equal
 * @param s1 First string
 * @param s2 Second string
 * @return 0 if both strings are equal, any other value if not
 */
int32_t stringCompare(const uint8_t * s1, const uint8_t * s2);
/**
 * @brief Returns the number of characters in the string
 * @param s String to determine its length
 * @return The number of characters in the string, withou null char
 */
int32_t stringLength(const uint8_t *s);
/**
 * @brief Copies the contents of a string to another
 */
void stringCopy(uint8_t * dest, uint8_t * src);
/**
 * @brief Converts an ascii character to a its represented value
 */
uint16_t getDigit(uint8_t c);

#endif /* SOURCE_STRINGUTILS_H_ */
