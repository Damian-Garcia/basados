/**
 * @file PortConfig.c
 * @brief This is the source file for the PortConfig header file
 */

#include "PortConfig.h"

#include "fsl_port.h"
#include "fsl_gpio.h"

void PortConfig_configUARTPins()
{
	PORT_SetPinMux(PORTB, 16, kPORT_MuxAlt3);
	PORT_SetPinMux(PORTB, 17, kPORT_MuxAlt3);

	PORT_SetPinMux(PORTE, 24, kPORT_MuxAlt2);
	PORT_SetPinMux(PORTE, 25, kPORT_MuxAlt2);

	PORT_SetPinMux(PORTC, 14, kPORT_MuxAlt3);
	PORT_SetPinMux(PORTC, 15, kPORT_MuxAlt3);

}

void PortConfig_configI2CPins()
{
	PORT_SetPinMux(PORTE, 24, kPORT_MuxAlt5);
	PORT_SetPinMux(PORTE, 25, kPORT_MuxAlt5);
}

void PortConfig_configSPIPins()
{
	PORT_SetPinMux(PORTD, 1, kPORT_MuxAlt2);
	PORT_SetPinMux(PORTD, 2, kPORT_MuxAlt2);

	PORT_SetPinMux(PORTD, 0, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTD, 3, kPORT_MuxAsGpio);

}

void PortConfig_configGPIOPins()
{

	PORT_SetPinMux(PORTC, 0, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTC, 1, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTC, 2, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTC, 3, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTC, 4, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTC, 5, kPORT_MuxAsGpio);

	gpio_pin_config_t config =
	{ kGPIO_DigitalInput, 1 };
	GPIO_PinInit(GPIOC, 0, &config);
	GPIO_PinInit(GPIOC, 1, &config);
	GPIO_PinInit(GPIOC, 2, &config);
	GPIO_PinInit(GPIOC, 3, &config);
	GPIO_PinInit(GPIOC, 4, &config);
	GPIO_PinInit(GPIOC, 5, &config);

	PORT_SetPinInterruptConfig(PORTC, 0, kPORT_InterruptFallingEdge);
	PORT_SetPinInterruptConfig(PORTC, 1, kPORT_InterruptFallingEdge);
	PORT_SetPinInterruptConfig(PORTC, 2, kPORT_InterruptFallingEdge);
	PORT_SetPinInterruptConfig(PORTC, 3, kPORT_InterruptFallingEdge);
	PORT_SetPinInterruptConfig(PORTC, 4, kPORT_InterruptFallingEdge);
	PORT_SetPinInterruptConfig(PORTC, 5, kPORT_InterruptFallingEdge);
}

void PortConfig_configAllPins()
{
	PortConfig_configUARTPins();
	PortConfig_configI2CPins();
	PortConfig_configSPIPins();
	PortConfig_configGPIOPins();
}
