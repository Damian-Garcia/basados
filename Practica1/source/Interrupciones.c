/**
 * @file Interrupciones.c
 * @brief This is the source file where IRQ_Handlers are implemented
 */

#include "fsl_uart.h"
#include "fsl_gpio.h"
#include "fsl_port.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "event_groups.h"

#include "IPC.h"
#include "Mensajes.h"
#include "Pantalla.h"

/**
 * @brief This is the IRQ Handler for the UART 0
 * It sends the new character to the input parser and to the lcd screen
 * if it is listenning.
 */
void UART0_RX_TX_IRQHandler()
{

	while (!(kUART_RxDataRegFullFlag & UART_GetStatusFlags(UART0)))
		;
	uint8_t c;
	c = UART_ReadByte(UART0);
	UART_WriteByte(UART0, c);

	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (Pantalla_isListening())
	{
		QueueHandle_t pantallaQueue = IPC_getLCDQueue();
		xQueueSendFromISR(pantallaQueue, &c, NULL);
	}

	QueueHandle_t q = IPC_getInputParser1Queue();
	xQueueSendFromISR(q, &c, &xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
/**
 * @brief This is the IRQ Handler for the UART 4 (Bluetooth)
 * It sends the new character to the input parser and to the lcd screen
 * if it is listenning.
 */
void UART4_RX_TX_IRQHandler()
{

	while (!(kUART_RxDataRegFullFlag & UART_GetStatusFlags(UART4)))
		;
	uint8_t c;
	c = UART_ReadByte(UART4);
	UART_WriteByte(UART4, c);

	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	if (Pantalla_isListening())
	{
		QueueHandle_t pantallaQueue = IPC_getLCDQueue();
		xQueueSendFromISR(pantallaQueue, &c, NULL);
	}

	QueueHandle_t q = IPC_getInputParser2Queue();
	xQueueSendFromISR(q, &c, &xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief This is the IRQ Handler for the GPIO C
 * It updates the local Time and Date objects  in the LCD module
 */
void PORTC_IRQHandler()
{

	GPIO_ClearPinsInterruptFlags(GPIOC, GPIO_GetPinsInterruptFlags(GPIOC));
	if (!GPIO_ReadPinInput(GPIOC, 0))
	{
		Pantalla_changeTime();
	}
	else if (!GPIO_ReadPinInput(GPIOC, 1))
	{
		Pantalla_changeDate();
	}
	else if (!GPIO_ReadPinInput(GPIOC, 2))
	{
		Pantalla_resume();
	}
	else if (!GPIO_ReadPinInput(GPIOC, 3))
	{
		Pantalla_incrementA();
	}
	else if (!GPIO_ReadPinInput(GPIOC, 4))
	{
		Pantalla_incrementB();
	}
	else if (!GPIO_ReadPinInput(GPIOC, 5))
	{
		Pantalla_incrementC();
	}
	else
	{

	}
}
