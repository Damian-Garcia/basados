/**
 * @file LeerFecha.h
 * @brief This header defines the functions for reading the date on the RTC
 */
#ifndef SOURCE_LEERFECHA_H_
#define SOURCE_LEERFECHA_H_

#include "stdbool.h"
#include "fsl_uart.h"
#include "FreeRTOS.h"
/**
 * @brief This function tries to read the date from the RTC
 * The function will try to communicate with the RTC via i2c
 * If the communication fails, the function will return false.
 * If communication was successful, it will print a success
 * message in the selected UART.
 * @param currentUart as UART to write the message
 * @return true if write operation was successful, false otherwise
 */
bool LeerFecha_tryToRead(UART_Type *currentUart);

#endif /* SOURCE_LEERFECHA_H_ */
