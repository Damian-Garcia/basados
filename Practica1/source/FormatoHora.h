/**
 * @file FormatoHora.h
 * @brief This header defines the functions for format changing in the RTC
 */
#ifndef SOURCE_FORMATOHORA_H_
#define SOURCE_FORMATOHORA_H_

#include "stdbool.h"
#include "fsl_uart.h"
#include "FreeRTOS.h"

/**
 * @brief This function tries to change the current format in the RTC
 * @param currentUart UART to print the results
 * @return true if it was possible to write, false otherwise
 */
bool FormatoHora_tryToUpdate(uint8_t *s, UART_Type *currentUart);

#endif /* SOURCE_FORMATOHORA_H_ */
