/**
 * @file I2CConfig.c
 * @brief This is the source file for the I2CConfig header file
 */
#include "I2CConfig.h"

i2c_master_handle_t g_m_handle;

volatile bool completionFlag;
volatile bool nakFlag;

static void i2c_master_callback(I2C_Type *base, i2c_master_handle_t *handle,
		status_t status, void *userData)
{
	/* Signal transfer success when received success status. */
	if (status == kStatus_Success)
	{
		completionFlag = true;
	}
	/* Signal transfer success when received success status. */
	if ((status == kStatus_I2C_Nak) || (status == kStatus_I2C_Addr_Nak))
	{
		nakFlag = true;
	}
}

void I2CConfig_init()
{
	i2c_master_config_t config_I2C =
	{ .enableMaster = true, .enableStopHold = false, .enableHighDrive = false,
			.baudRate_Bps = 100000, .glitchFilterWidth = 0 };

	I2C_MasterInit(I2C0, &config_I2C, CLOCK_GetFreq(I2C0_CLK_SRC));

	I2C_MasterTransferCreateHandle(I2C0, &g_m_handle, i2c_master_callback,
			NULL);

	I2C_MasterInit(I2C0, &config_I2C, CLOCK_GetFreq(I2C0_CLK_SRC));
	I2C_MasterTransferCreateHandle(I2C0, &g_m_handle, i2c_master_callback,
			NULL);
}

bool I2CConfig_WriteReg(uint8_t slaveAdd, I2C_Type* I2C_base, uint32_t reg_addr,
		uint8_t value)
{
	i2c_master_transfer_t masterXfer;
	memset(&masterXfer, 0, sizeof(masterXfer));

	masterXfer.slaveAddress = slaveAdd;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = reg_addr;
	masterXfer.subaddressSize = 1;
	masterXfer.data = &value;
	masterXfer.dataSize = 1;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C_base, &g_m_handle, &masterXfer);

	/*  wait for transfer completed. */
	while ((!nakFlag) && (!completionFlag))
	{
	}

	nakFlag = false;

	if (completionFlag == true)
	{
		completionFlag = false;
		return true;
	}
	else
	{
		return false;
	}
}

bool I2CConfig_ReadReg(uint8_t slaveAdd, I2C_Type* I2C_base, uint32_t reg_addr,
		uint8_t *rxBuff, uint32_t rxSize)
{
	i2c_master_transfer_t masterXfer;
	memset(&masterXfer, 0, sizeof(masterXfer));
	masterXfer.slaveAddress = slaveAdd;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = reg_addr;
	masterXfer.subaddressSize = 1;
	masterXfer.data = rxBuff;
	masterXfer.dataSize = rxSize;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C_base, &g_m_handle, &masterXfer);

	/*  wait for transfer completed. */
	while ((!nakFlag) && (!completionFlag))
	{
	}

	nakFlag = false;

	if (completionFlag == true)
	{
		completionFlag = false;
		return true;
	}
	else
	{
		return false;
	}
}

bool I2CConfig_WriteBuffer(uint8_t slaveAdd, I2C_Type* I2C_base,
		uint16_t begin_addr, uint8_t* value, uint32_t len)
{
	i2c_master_transfer_t masterXfer;
	memset(&masterXfer, 0, sizeof(masterXfer));

	masterXfer.slaveAddress = slaveAdd;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = begin_addr;
	masterXfer.subaddressSize = 2;
	masterXfer.data = value;
	masterXfer.dataSize = len;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C_base, &g_m_handle, &masterXfer);

	/*  wait for transfer completed. */
	while ((!nakFlag) && (!completionFlag))
	{
	}

	nakFlag = false;

	if (completionFlag == true)
	{
		completionFlag = false;
		return true;
	}
	else
	{
		return false;
	}
}

bool I2CConfig_ReadBuffer(uint8_t slaveAdd, I2C_Type* I2C_base,
		uint16_t begin_addr, uint8_t *rxBuff, uint32_t rxSize, uint32_t len)
{
	i2c_master_transfer_t masterXfer;
	memset(&masterXfer, 0, sizeof(masterXfer));

	masterXfer.slaveAddress = slaveAdd;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = begin_addr;
	masterXfer.subaddressSize = 2;
	masterXfer.data = rxBuff;
	masterXfer.dataSize = len;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferNonBlocking(I2C_base, &g_m_handle, &masterXfer);
	/*  wait for transfer completed. */
	while ((!nakFlag) && (!completionFlag))
	{
	}

	nakFlag = false;

	if (completionFlag == true)
	{
		completionFlag = false;
		return true;
	}
	else
	{
		return false;
	}
}
