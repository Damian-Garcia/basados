/**
 * @file ClockConfig.h
 * @brief This header defines the functions for clock gating of internal modules
 */
#ifndef SOURCE_CLOCKCONFIG_H_
#define SOURCE_CLOCKCONFIG_H_

/**
 * @brief This function enables all necesary clock gating.
*/
void ClockConfig_configAllClocks();
/**
 * @brief This function enables clock gating for the gpio module
 */
void ClockConfig_configGPIOClock();
/**
 * @brief This function enables clock gating for the uart module
 */
void ClockConfig_configUARTClock();
/**
 * @brief This function enables clock gating for the i2c module
 */
void ClockConfig_configI2CClock();
/**
 * @brief This function enables clock gating for the spi module
 */
void ClockConfig_configSPIClock();

#endif /* SOURCE_CLOCKCONFIG_H_ */
