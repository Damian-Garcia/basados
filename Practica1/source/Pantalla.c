/**
 * @file Pantalla.c
 * @brief This is the source file for the Pantalla header file
 */
#include "stdbool.h"
#include "Pantalla.h"
#include "RTC.h"
#include "Formatter.h"

Time localTime;
Date localDate;

bool shouldUpdate = true;

uint8_t hora_fecha;
bool listening = false;

void Pantalla_pause()
{
	shouldUpdate = false;
}

void Pantalla_changeTime()
{
	Pantalla_pause();
	hora_fecha = 0;
}

void Pantalla_changeDate()
{
	Pantalla_pause();
	hora_fecha = 1;
}

void Pantalla_resume()
{
	shouldUpdate = true;

	uint8_t bhoras[4], bminutos[4], bsegundos[4];
	uint8_t bdias[4], bmeses[4], banos[4];

	formatUnsignedInteger32(localTime.h, bhoras, 2);
	formatUnsignedInteger32(localTime.m, bminutos, 2);
	formatUnsignedInteger32(localTime.s, bsegundos, 2);

	formatUnsignedInteger32(localDate.d, bdias, 2);
	formatUnsignedInteger32(localDate.m, bmeses, 2);
	formatUnsignedInteger32(localDate.a, banos, 2);

	localTime.h = (bhoras[0] & 0xf) << 4 | (bhoras[1] & 0xf);
	localTime.m = (bminutos[0] & 0xf) << 4 | (bminutos[1] & 0xf);
	localTime.s = (bsegundos[1] & 0xf) << 4 | (bsegundos[1] & 0xf);

	localDate.d = (bdias[0] & 0xf) << 4 | (bdias[1] & 0xf);
	localDate.m = (bmeses[0] & 0xf) << 4 | (bmeses[1] & 0xf);
	localDate.a = (banos[0] & 0xf) << 4 | (banos[1] & 0xf);

	RTC7940_updateHora(&localTime);
	RTC7940_updateFecha(&localDate);
}

void Pantalla_incrementA()
{
	if (!hora_fecha)
	{
		localTime.s++;
		localTime.s %= 60;
	}
	else
	{
		localDate.d++;
		localDate.d %= 31;
	}
}

void Pantalla_incrementB()
{
	if (!hora_fecha)
	{
		localTime.m++;
		localTime.m %= 60;
	}
	else
	{
		localDate.m++;
		localDate.m %= 12;

	}
}

void Pantalla_incrementC()
{
	if (!hora_fecha)
	{
		localTime.h++;
		localTime.h %= 12;
	}
	else
	{
		localDate.a++;
		localDate.a %= 100;

	}
}

void printHoraLCD()
{
	uint8_t hB[5], sB[5], mB[5];
	formatUnsignedInteger32(localTime.h, hB, 2);
	formatUnsignedInteger32(localTime.m, mB, 2);
	formatUnsignedInteger32(localTime.s, sB, 2);

	LCDNokia_sendString("Hora: ");
	LCDNokia_sendString(hB);
	LCDNokia_sendString(":");
	LCDNokia_sendString(mB);
	LCDNokia_sendString(":");
	LCDNokia_sendString(sB);
	LCDNokia_sendString(" ");
	if (localTime.f && !(localTime.h > 12))
	{
		LCDNokia_sendString("am");
	}
	if (localTime.f && (localTime.h > 12))
	{
		LCDNokia_sendString("pm");
	}
}

void printFechaLCD()
{
	uint8_t aB[5], mB[5], dB[5];
	formatUnsignedInteger32(localDate.a, aB, 2);
	formatUnsignedInteger32(localDate.m, mB, 2);
	formatUnsignedInteger32(localDate.d, dB, 2);

	LCDNokia_sendString("Fecha : ");
	LCDNokia_sendString(dB);
	LCDNokia_sendString("/");
	LCDNokia_sendString(mB);
	LCDNokia_sendString("/20");
	LCDNokia_sendString(aB);

}

void tryToUpdate()
{
	uint8_t s = 0, m = 0, h = 0, f = 0;
	uint8_t bs[5] =
	{ };
	uint8_t bm[5] =
	{ };
	uint8_t bh[5] =
	{ };

	bool res = true;
	if (shouldUpdate)
	{
		res = RTC7940_readHora(&localTime);
		res &= RTC7940_readFecha(&localDate);
	}

	if (res)
	{
		printHoraLCD();
		printFechaLCD();
	}
	else
	{
		LCDNokia_sendString("No hay rtc");
	}

}

bool Pantalla_isListening()
{
	return listening;
}

void Pantalla_listen(bool l)
{
	LCDNokia_clear();
	listening = l;
}

void PantallaUpdate_taks(void *p)
{
	QueueHandle_t q = IPC_getLCDQueue();
	uint8_t c;
	for (;;)
	{
		if (!listening)
		{
			LCDNokia_clear();
			LCDNokia_gotoXY(0, 0);
			tryToUpdate();
			vTaskDelay(500);
		}
		else
		{
			xQueueReceive(q, &c, portMAX_DELAY);
			LCDNokia_sendChar(c);
		}
	}

}

