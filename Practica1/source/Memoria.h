/**
 * @file Memoria.h
 * @brief This header defines an API to interface the external memory
 */
#ifndef SOURCE_MEMORIA_H_
#define SOURCE_MEMORIA_H_

#include "stdbool.h"
#include "fsl_i2c.h"

/**
 * @brief Read Memory Interface method
 * @param direccion address to read as an in integer
 * @param cont number of bytes to read, as an integer
 * @param buffer buffer to store the read bytes
 * @return true if read operation was successful
 */
bool Memoria_leer(uint32_t direccion, uint32_t cont, uint8_t *buffer);
/**
 * @brief Write Memory Interface method
 * @param direccion address to read as an in integer
 * @param cont number of bytes to read, as an integer
 * @param buffer buffer to store the read bytes
 * @return true if read operation was successful
 */
bool Memoria_escribir(uint32_t direccion, uint32_t cont, uint8_t *buffer);

#endif /* SOURCE_MEMORIA_H_ */
